Origin: upstream, https://github.com/zyantific/zycore-c/pull/38/commits/e0d4dd3571aefd014d3d9dd714468a3241026693
Forwarded: https://github.com/zyantific/zycore-c/pull/38
Last-Update: 2021-11-12

From e0d4dd3571aefd014d3d9dd714468a3241026693 Mon Sep 17 00:00:00 2001
From: Andrea Pappacoda <andrea@pappacoda.it>
Date: Fri, 12 Nov 2021 17:04:33 +0100
Subject: [PATCH] build: use system GTest when available

This reduces compile times and allows tests to be ran in environments
without internet access (e.g. when building a Debian package).

CMake will look for an installed copy of GoogleTest on the system, and
will use it if found, and will fallback to download and unpack it
otherwise.

There are a lot of checks involved since CMake changed the way FindGTest
works in newer CMake versions, and they are needed to ensure 3.1
compatibility.
---
 CMakeLists.txt | 55 +++++++++++++++++++++++++++++++++-----------------
 1 file changed, 36 insertions(+), 19 deletions(-)

diff --git a/CMakeLists.txt b/CMakeLists.txt
index 90e495d..97ea83d 100644
--- a/CMakeLists.txt
+++ b/CMakeLists.txt
@@ -63,31 +63,48 @@ endif ()
 # GoogleTest                                                                                      #
 # =============================================================================================== #
 
-# Download and unpack googletest
+# Search for GoogleTest, and fallback to downloading it if not found
 if (ZYCORE_BUILD_TESTS)
-    if (NOT DEFINED ZYCORE_DOWNLOADED_GTEST)
-        configure_file("CMakeLists.txt.in" "${CMAKE_BINARY_DIR}/gtest/download/CMakeLists.txt")
-        execute_process(COMMAND ${CMAKE_COMMAND} -G "${CMAKE_GENERATOR}" .
-            RESULT_VARIABLE result
-            WORKING_DIRECTORY "${CMAKE_BINARY_DIR}/gtest/download")
-        if (result)
-            message(FATAL_ERROR "CMake step for googletest failed: ${result}")
-        endif()
-        execute_process(COMMAND ${CMAKE_COMMAND} --build .
-            RESULT_VARIABLE result
-            WORKING_DIRECTORY "${CMAKE_BINARY_DIR}/gtest/download")
-        if (result)
-            message(FATAL_ERROR "Build step for googletest failed: ${result}")
-        endif()
-
-        set(ZYCORE_DOWNLOADED_GTEST TRUE CACHE BOOL "")
-        mark_as_advanced(ZYCORE_DOWNLOADED_GTEST)
-    endif ()
+    find_package(GTest QUIET)
+    if (GTest_FOUND)
+        # CMake 3.20 and upstream GTestConfig.cmake
+        if (TARGET GTest::gtest)
+            add_library(gtest ALIAS GTest::gtest)
+        # CMake 3.5 and newer
+        elseif (TARGET GTest::GTest)
+            add_library(gtest ALIAS GTest::GTest)
+        # Older CMake versions
+        else ()
+            add_library(gtest INTERFACE)
+            find_package(Threads REQUIRED)
+            target_include_directories(gtest INTERFACE "${GTEST_INCLUDE_DIRS}")
+            target_link_libraries(gtest INTERFACE "${GTEST_LIBRARIES}" Threads::Threads)
+        endif ()
+    else ()
+        if (NOT DEFINED ZYCORE_DOWNLOADED_GTEST)
+            configure_file("CMakeLists.txt.in" "${CMAKE_BINARY_DIR}/gtest/download/CMakeLists.txt")
+            execute_process(COMMAND ${CMAKE_COMMAND} -G "${CMAKE_GENERATOR}" .
+                RESULT_VARIABLE result
+                WORKING_DIRECTORY "${CMAKE_BINARY_DIR}/gtest/download")
+            if (result)
+                message(FATAL_ERROR "CMake step for googletest failed: ${result}")
+            endif()
+            execute_process(COMMAND ${CMAKE_COMMAND} --build .
+                RESULT_VARIABLE result
+                WORKING_DIRECTORY "${CMAKE_BINARY_DIR}/gtest/download")
+            if (result)
+                message(FATAL_ERROR "Build step for googletest failed: ${result}")
+            endif()
+
+            set(ZYCORE_DOWNLOADED_GTEST TRUE CACHE BOOL "")
+            mark_as_advanced(ZYCORE_DOWNLOADED_GTEST)
+        endif ()
 
     set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)
 
     add_subdirectory("${CMAKE_BINARY_DIR}/gtest/src" "${CMAKE_BINARY_DIR}/gtest/build"
         EXCLUDE_FROM_ALL)
+    endif ()
 endif ()
 
 list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/cmake")
